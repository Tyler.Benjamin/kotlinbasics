package com.example.kotlinbasics

fun calculateSum() {
    val firstNumber = 10
    val secondNumber = 5
    val result = firstNumber + secondNumber

    println("$firstNumber + $secondNumber = $result")
}


fun calculateSum2(firstNumber: Int, secondNumber: Int) {
    val result = firstNumber + secondNumber

    println("$firstNumber + $secondNumber = $result")
}
